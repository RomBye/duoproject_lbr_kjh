#ifndef __LIFE_H__
#define __LIFE_H__
#include "cocos2d.h"
using namespace cocos2d;

class Creature{
private:
	//알과 다큰애들의 sprite들
	//세이브 버튼과 뒤로가기 버튼들
	Sprite* Creature[6];
public:
	
	/**DefineObject
	*Creature의 모든 object를 생성하는 함수
	*button과 sprite모두 생성을 하는 함수이다.
	*/
	void CreateObject(Layer*);

	/**addClickEventListener
	*CreateObject에서 Statebutton의 CLickEventLiserer을 담당하는 함수들이다.
	*/
	void ButtonCleaningClickEvent(Ref*);
	void ButtonMealClickEvent(Ref*);
	void ButtonPlayClickEvent(Ref*);
	void ButtonShowerClickEvent(Ref*);
	void ButtonSleepClickEvent(Ref*);
	
	/**Storing data of a Creature
	*/
	void ButtonSaveClickEvent(Ref*);

};
/** Makes a Simple Buttons
간단하게 EventListener을가진 Button을 만드는 함수
*/
void CreateButton(char*, char*, Layer*, Vec2, std::function<void(Ref*)>);
/** Makes a Simple Sprite
간단하게 Sprite를 만드는 함수
*/
void CreateSprite(Sprite* , char* , Vec2 , Layer* );

#endif