#include "cocos2d.h"
#ifndef __ALGORITHM_H__
#define __ALGORITHM_H__

	using namespace cocos2d;

		//상태가 변할경우 그에 맞는 그림으로 맞춰놓는 함수 필요
		//최대 최소 치에 따른 결과 값들

		//현재 생명체의 기분과 환경을 숫자로 나타내는 구조체
		typedef struct CollectionIntState{
			//Happy State
			int HappyState = 0;
			//Bad State
			int HungryState = 0;
			int DirtyState = 0;
			int TiredState = 0;
			int PutState = 0;
			int* StateArray[5];
			void StateArrayDefine(){
				int* StateArrayOverwrite[] = { &HappyState, &HungryState, &DirtyState, &TiredState, &PutState };
				for (int i = 0; i < 5; i++) StateArray[i] = StateArrayOverwrite[i];
 			}
		}CollectionIntState;

		//StateSizeCollection의 사용되는 열거형 : 현재 정도를 표시해주는 열거형
		//총 3단계이다. 처음시작은 모두 Normal부터 시작한다.
		enum StateSize{ Normal = 0, Very, fouck };

		//현재 생명체의 기분과 환경의 기준치를 넘을경우 현재 상태를 채크해놓는 구조체
		typedef struct CollectionStateSize{
			StateSize HappySize = StateSize::Normal;
			StateSize HungrySize = StateSize::Normal;
			StateSize DirtySize = StateSize::Normal;
			StateSize TiredSize = StateSize::Normal;
			StateSize PutSize = StateSize::Normal;

			StateSize* SizeArray[5];
			void StateSizeArrayDefine(){
				StateSize* SizeArrayOverwrite[] = { &HappySize, &HungrySize, &DirtySize, &TiredSize, &PutSize };
				for (int i = 0; i < 5; i++) SizeArray[i] = SizeArrayOverwrite[i];
			}
		}CollectionStateSize;

		//Bird의 Sprite들을 가지고 있는 구조체
		typedef struct CollectionSpriteBodyPart{
			Sprite* Char_Mouth;
			Sprite* Char_Hand; 
			Sprite* Char_Body; 
			Sprite* Char_Tired;
			Sprite* Char_Put;
			Sprite* Char_Eye;
			Sprite** Char_SpriteArray[5];
			void StateSpriteArrayDefine(){
				Sprite** OverwriteArray[5] = { &Char_Mouth, &Char_Hand, &Char_Body, &Char_Tired, &Char_Put };
				for (int i = 0; i < 5; i++) Char_SpriteArray[i] = OverwriteArray[i];
			}
		}CollectionSpriteBodyPart;
		class Life
		{
		private:

			/////////////////////////////////////////////// 캐릭터의 상태를 채크하는 변수/////////////////////////////////////////////// 

			//살아있는지 체크하는 변수
			bool LifeFlesh = true;

			//Life의 캐릭터의 상태를 정수의 형태로 받는 변수
			CollectionIntState LifeStateVariables;
			
			//Life의 캐릭터의 정수를 받아 정도를 적어놓는 변수
			CollectionStateSize LifeStateSize;

			//한계치를 설정해둔 변수
			int Limit_Value[3];

			//눈의 운동을 담은 캐릭터
			int Lifeflicker = 0;

			//알인지 아닌지를 체크하는 변수
			bool Lifeeggstate = true;

			//알의 체력을 나타내는 변수
			int LifeeggHp;

			/////////////////////////////////////////////// 인터페이스 변수/////////////////////////////////////////////// 

			//세이브 버튼
			ui::Button* SaveBu;

			/*State를 채워주는 Button들*/
			ui::Button* Bu[5];

			/////////////////////////////////////////////// sprite 변수/////////////////////////////////////////////// 

			//egg의 sprite
			Sprite* LifeeggSprite;

			//Life Sprite들을 담은 변수
			CollectionSpriteBodyPart LifeStateSprite;

			/////////////////////////////////////////////// 추가적으로 필요한 변수/////////////////////////////////////////////// 

			//상태를 차감되는 정도를 지정하는 변수
			int Deduction[4];

			//bird의 spritename
			char* CarryAcross[5];

			//눈을 움직임이게 해주는 변수
			int LifeflickerDe = 1;

			//눈 깜빡임의 딜레이를 주는 변수
			int Count = 0;

		public:

			Life(){
				LifeStateVariables.StateArrayDefine();
				LifeStateSize.StateSizeArrayDefine();
				LifeStateSprite.StateSpriteArrayDefine();
				LifeCarryAcrossDefine();
				Deduction[0] = 2;
				Deduction[1] = 2;
				Deduction[2] = 2;
				Deduction[3] = 2;
				Limit_Value[0] = 10;
				Limit_Value[1] = 50;
				Limit_Value[2] = 100;
				LifeeggHp = 10;
			}

			/********변수들 정의하는 함수들********/

			//이름 초기화 해주는 함수
			void LifeCarryAcrossDefine();

			//Button을 위치와 포지션을 잡는 함수
			void LifeButtonDefine(Layer*);

			//Sprite를 한다.
			void LifeSpriteBodyDefine(Layer*);

			void LifeSpriteName();

			//lifeegg를 정의하는 함수
			void LifeeggDefine(Layer*);
			
			/********scheulde함수들********/

			//Algorithm을 돌리는 함수들
			void LifeTotalSchedule();
			void LifeOverSchedule();

			//눈 깜박임 표현 함수
			void LifeEyerflicker();

			//사망시 나오고 모든 schedule을 정지시키는 함수
			void LifeDiedScene(Layer*);

			/********데이터관련 함수들********/

			//저장하는 함수
			void LifeDataSave();

			//데이터를 받는 함수
			void LifeDataReceive();
			
			//모든 최대치 최소치를보고 값을 넘기는 것
			void ChackMinMax();

			/********bind될 함수들********/

			//button을 누를 경우 State의 값을 올려주는 함수
			void ButtonClickHungry();
			void ButtonClickDirty();
			void ButtonClickTired();
			void ButtonClickPut();
			void ButtonClickPlay();

			//알을 깨는 함수
			bool SpriteClickegg();

			/********return 함수********/

			//알상태의 체력을 보여주는 함수
			int LifereturneggHpState();

			//Life Flesh반환 함수
			bool getLifeFlesh();
		};
		void returnfilename();
		//차감과 변경을 하는 함수
		void TotalAutomaticState(int[], int*[], StateSize*[]);
		//현재 수치를 본 후  State의 Size들을 변경하는 것이다.
		bool Over_State_Change(int *[], StateSize*[], int*, Sprite***, char**);
		//캐릭터의 이름을 받아 바꿔놓는 함수
		void Sprtie_Change(Sprite*, char*,int );
		
#endif