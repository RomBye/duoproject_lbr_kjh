#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "Algorithm.h"

using namespace cocos2d;

class GameScene : public cocos2d::Layer{
public:
	///////////////////scenebasic/////////////////// 

	static Scene* createScene(bool);
	virtual bool init();
	virtual void onEnter();

	///////////////////schedule/////////////////// 

	void Change(float);
	void Time(float);
	void move(float);

	///////////////////EvnetLisner/////////////////// 
	bool eggTouchEvnet(Touch*, Event*);
	
	///////////////////variable/////////////////// 
	EventListenerTouchOneByOne* touchegg;
	bool GameStartState;
	Life Bird;

	CREATE_FUNC(GameScene);
};

#endif