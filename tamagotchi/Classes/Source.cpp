#include "ui\CocosGUI.h"
#include "Header.h"
#define PositionX(X) (500+(X))
#define PositionY(Y) (400+(Y))

void Creature::CreateObject(Layer*Sceneinherit){
	char* ButtonFile_Name[5] = { "image/ButtonCleaning.png", "image/ButtonMeal.png", "image/ButtonPlay.png", "image/ButtonShower.png", "image/ButtonSleep.png" };
	std::function<void(Ref*)> Buttonfuction[5] = { ButtonCleaningClickEvent, ButtonMealClickEvent, ButtonPlayClickEvent, ButtonShowerClickEvent, ButtonSleepClickEvent };
	for (int i = 0; i < 5; i++){
		CreateButton(ButtonFile_Name[i], NULL, Sceneinherit, Vec2(200 + (i * 200), 100), Buttonfuction[i]);
	}
	CreateButton("btn_up.png", "Save", Sceneinherit, Vec2(900, 900), ButtonSaveClickEvent);
	char* Spritfilename[6] = { "image/BirdBody0.png", "image/BirdEye0.png", "image/BirdHand0.png", "image/BirdMouth0.png", "image/BirdTired0.png", "image/BirdPut0.png" };
	Vec2 SpritePosition[6] = { Vec2(PositionX(0), PositionY(0)), Vec2(PositionX(10), PositionY(100)), Vec2(PositionX(-130), PositionY(-50)), Vec2(PositionX(50), PositionY(0)), Vec2(PositionX(14), PositionY(75)), Vec2(PositionX(500), PositionY(0)) };
		for (int i = 0; i < 6; i++){
			CreateSprite(Creature[i],Spritfilename[i], SpritePosition[i], Sceneinherit);
	}
}

void Creature::ButtonCleaningClickEvent(Ref*){}
void Creature::ButtonMealClickEvent(Ref*){}
void Creature::ButtonPlayClickEvent(Ref*){}
void Creature::ButtonShowerClickEvent(Ref*){}
void Creature::ButtonSleepClickEvent(Ref*){}

void Creature::ButtonSaveClickEvent(Ref*){}

void CreateButton(char* Buttonfilename, char* textname, Layer* Sceneinherit, Vec2 Position, std::function<void(Ref*)> function){
	ui::Button* button = ui::Button::create(Buttonfilename);
	if (textname != NULL)button->setTitleText(textname);
	button->setPosition(Position);
	Sceneinherit->addChild(button);
	button->addClickEventListener(std::bind(function, Sceneinherit));
}
void CreateSprite(Sprite* sprite, char* Spritefilename, Vec2 psition, Layer* Sceneinherit){
	sprite = Sprite::create(Spritefilename);
	sprite->setPosition(psition);
	Sceneinherit->addChild(sprite);
}