

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Option.h"

//enum Thema { RED = 1, GREEN, BLUE };

using namespace cocos2d;
class HelloWorld : public cocos2d::Layer
{
public:
	Label* version = nullptr;
	ui::Button* mainButton[4];
	ui::Layout* mainLayout= nullptr;
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    // implement the "static create()" method manually
	virtual void onEnter();
	CREATE_FUNC(HelloWorld);

	//version의 데이터를 받아 문자로 표현해주는 함수
	void versionGrapher();

	//메인 레이어와 메인 버튼 생성함수
	void createButtons();

	//옵션버튼 클릭시 호출되는 함수
	void optionButtonClick();

	//옵션창을 닫을때 호출되는 함수
	void optionClear();

};

#endif // __HELLOWORLD_SCENE_H__
