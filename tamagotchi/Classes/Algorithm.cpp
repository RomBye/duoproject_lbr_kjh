#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Algorithm.h"
#include "HelloWorldScene.h"

///////////////////////////////////////////////////// Algorithm define /////////////////////////////////////////////////////
//포인터 배열의 크기를 정확하게 아는 법
#define Size(X) (sizeof(X) / sizeof(int))
#define PositionX 500
#define PositionY 400

/*
알부터 시작하는 건 스케줄을 한번만 돌리는 것으로 버튼과 캐릭터들을 만들고 파티클을 이용하자
*/
//백버튼
//추가적으로 더 캐릭터를 생성해도 될수있게 좀더 부드럽게 만들자

void Life::LifeTotalSchedule(){
	TotalAutomaticState(Deduction, LifeStateVariables.StateArray, LifeStateSize.SizeArray);
}
void Life::LifeOverSchedule(){
	Life::LifeFlesh = Over_State_Change(LifeStateVariables.StateArray, LifeStateSize.SizeArray, Limit_Value,
		LifeStateSprite.Char_SpriteArray, CarryAcross);
}

///////////////////////////////////////////////// Life Class Return Variable /////////////////////////////////////////////////

bool Life::getLifeFlesh(){ return LifeFlesh; }
int Life::LifereturneggHpState(){ return LifeeggHp; }

//////////////////////////////////////////////////// Life Class Argorithm ////////////////////////////////////////////////////

//데이터 세이브
void Life::LifeDataSave(){
	FILE* pushData = fopen("LifeData.txt", "wt");
	for (int i = 0; i < 5; i++){
		fprintf(pushData, "%d ", *(LifeStateVariables.StateArray[i]));
	}
	fclose(pushData);
}


//파일이 없을 경우를 만들자
void Life::LifeDataReceive(){
	int d = 0;
	FILE* pullData = fopen("LifeData.txt", "rt");
	for (int i = 0; i < 5; i++){
		fscanf(pullData, "%d ", LifeStateVariables.StateArray[i]);
	}
	fclose(pullData);
}
void Life::LifeEyerflicker(){
	if (Count == 0){
		Lifeflicker += LifeflickerDe;
		std::stringstream name;
		name << "image/BirdEye" << Lifeflicker << ".png";
		std::string getname = name.str();
		LifeStateSprite.Char_Eye->setTexture(Director::getInstance()->getTextureCache()->addImage(getname.c_str()));
		if (Lifeflicker == 5) LifeflickerDe = -1;
		else if (Lifeflicker == 0){
			LifeflickerDe = 1;
			Count = 200;
		}
	}
	else {
		Count -= 1;
	}
}


/////////////////////////////////////////////// Life Class Variable Define ///////////////////////////////////////////////////


void Life::LifeDiedScene(Layer* Scene){
	ui::TextField* DiedText = ui::TextField::create("Pets Died", "04B_21__.TTF", 20.0f);
	DiedText->setPosition(Vec2(500, 500));
	DiedText->setTextColor(Color4B(0, 0, 0, 0));
	Scene->addChild(DiedText);

	ui::Button* button = ui::Button::create("image/btn_up.png");
	button->setPosition(Vec2(500, 380));
	Scene->addChild(button);
	button->addClickEventListener([](Ref* rf)->void{ Director::getInstance()->replaceScene(HelloWorld::createScene()); });

	Scene->unscheduleAllSelectors();
}

void Life::LifeButtonDefine(Layer* layer){
	Bu[0] = ui::Button::create("image/ButtonMeal.png");
	Bu[1] = ui::Button::create("image/ButtonShower.png");
	Bu[2] = ui::Button::create("image/ButtonSleep.png");
	Bu[3] = ui::Button::create("image/ButtonCleaning.png");
	Bu[4] = ui::Button::create("image/ButtonPlay.png");
	for (int i = 0; i < 5; i++){
		Life::Bu[i]->setPosition(Vec2(200 * (i + 1), 100));
		layer->addChild(Life::Bu[i]);
	}
	SaveBu = ui::Button::create("image/btn_up.png");
	SaveBu->setTitleText("Save");
	SaveBu->setPosition(Vec2(1000, 700));
	layer->addChild(SaveBu);
	SaveBu->addClickEventListener(std::bind(&Life::LifeDataSave, this));
	Bu[0]->addClickEventListener(std::bind(&Life::ButtonClickHungry, this));
	Bu[1]->addClickEventListener(std::bind(&Life::ButtonClickDirty, this));
	Bu[2]->addClickEventListener(std::bind(&Life::ButtonClickTired, this));
	Bu[3]->addClickEventListener(std::bind(&Life::ButtonClickPut, this));
	Bu[4]->addClickEventListener(std::bind(&Life::ButtonClickPlay, this));
}

void Life::LifeSpriteBodyDefine(Layer* layer){
	returnfilename();
	Life::LifeStateSprite.Char_Body = Sprite::create("image/BirdBody0.png");
	Life::LifeStateSprite.Char_Eye = Sprite::create("image/BirdEye0.png");
	Life::LifeStateSprite.Char_Hand = Sprite::create("image/BirdHand0.png");
	Life::LifeStateSprite.Char_Mouth = Sprite::create("image/BirdMouth0.png");
	Life::LifeStateSprite.Char_Tired = Sprite::create("image/BirdTired0.png");
	Life::LifeStateSprite.Char_Put = Sprite::create("image/BirdPut0.png");
	Life::LifeStateSprite.Char_Body->setPosition(Vec2(PositionX, PositionY));
	Life::LifeStateSprite.Char_Eye->setPosition(Vec2(PositionX + 10, PositionY + 100));
	Life::LifeStateSprite.Char_Hand->setPosition(Vec2(PositionX - 130, PositionY - 50));
	Life::LifeStateSprite.Char_Mouth->setPosition(Vec2(PositionX + 50, PositionY));
	Life::LifeStateSprite.Char_Tired->setPosition(Vec2(PositionX + 14, PositionY + 75));
	Life::LifeStateSprite.Char_Put-> setPosition(Vec2(PositionX + 500, PositionY));
	layer->addChild(Life::LifeStateSprite.Char_Body);
	layer->addChild(Life::LifeStateSprite.Char_Mouth);
	layer->addChild(Life::LifeStateSprite.Char_Hand);
	layer->addChild(Life::LifeStateSprite.Char_Tired);
	layer->addChild(Life::LifeStateSprite.Char_Put);
	layer->addChild(Life::LifeStateSprite.Char_Eye);
}

void Life::LifeeggDefine(Layer* layer){
	Life::LifeeggSprite = Sprite::create("image/Egg0.png");
	Life::LifeeggSprite->setPosition(Vec2(PositionX, PositionY));
	layer->addChild(Life::LifeeggSprite);
}

void Life::LifeCarryAcrossDefine(){
	CarryAcross[0] = "image/BirdMouth";
	CarryAcross[1] = "image/BirdHand";
	CarryAcross[2] = "image/BirdBody";
	CarryAcross[3] = "image/BirdTired";
	CarryAcross[4] = "image/BirdPut";
}

/////////////////////////////////////////////////////// Life bind Class //////////////////////////////////////////////////////

void Life::ButtonClickHungry(){ CCLOG("Hungry"); if (Life::LifeStateVariables.HungryState - 2 > 0) Life::LifeStateVariables.HungryState -= 2; }
void Life::ButtonClickDirty(){ CCLOG("Dirty"); if (Life::LifeStateVariables.DirtyState - 2 > 0) Life::LifeStateVariables.DirtyState -= 2; }
void Life::ButtonClickTired(){ CCLOG("Tired"); if (Life::LifeStateVariables.TiredState - 2 > 0) Life::LifeStateVariables.TiredState -= 2; }
void Life::ButtonClickPut(){ CCLOG("Puts"); if (Life::LifeStateVariables.PutState - 2 > 0) Life::LifeStateVariables.PutState -= 2; }
void Life::ButtonClickPlay(){ CCLOG("Play"); if (Life::LifeStateVariables.HappyState - 2 > 0)  Life::LifeStateVariables.HappyState -= 2; }
bool Life::SpriteClickegg(){
	CCLOG("egg = %d", LifeeggHp);
	if (LifeeggHp >0){
		LifeeggHp -= 1;
		return true;
	}
	else{
		LifeeggSprite->removeFromParentAndCleanup(true);
		return false;
	}
}

//////////////////////////////////////////////////////nat Class Function//////////////////////////////////////////////////////

void  TotalAutomaticState(int num[], int* State[], StateSize* StateSize[]){
	for (int i = 1; i < 5; i++){
		*(State[i]) += num[i-1];
	}
	//Happy의 차감 정도
	int Deduction = 0;
	CCLOG("State.HungryState %d", *(State[1]));
	CCLOG("State.DirtyState %d", *(State[2]));
	CCLOG("State.TiredState %d", *(State[3]));
	CCLOG("State.PutState %d", *(State[4]));
	// i 를 1로 초기화한것은 StateAll의 변수가 HappyState도 포함되어있기 때문이다.
	for (int i = 1; i < 5; i++){
		//상태를 표시하는 StateAll가 true일 경우 그 값만큼 Deduction을 뺀다.
		for (int k = 0; k < 3 && *(StateSize[i]) > k; k++){
			Deduction += 8;
		}
	}

	*(State[0]) += Deduction;
	CCLOG("State.HappyState %d", *(State[0]));
	CCLOG("ssssssssssssssssssssssss");
}
/*LifeStateVariables.StateArray, LifeStateSize.SizeArray, Limit_Value, LifeStateSprite.Char_SpriteArray, CarryAcross*/
bool Over_State_Change(int * Pobject[], StateSize* StateAll[], int* Step, Sprite*** sprite, char** pstring){
	for (int i = 0; i < 5; i++){
		int num = 0;
		for (int k = 0; k < 3 && *(Pobject[i]) >= *(Step + k); k++){
			num = k;
			if (k == 2 && i > 0){
				return false;
			}
		}
		*(StateAll[i]) = (StateSize)num;
		//&Char_Mouth, &Char_Hand, &Char_Body, &Char_Tired, &Char_Put  서순
		Sprtie_Change(*(sprite[i]), pstring[i], *(StateAll[i]));
	}
	return true;
}
void Sprtie_Change(Sprite* sp, char* ch,int num){
	if (num > 2){
		CCLOG("SpriteChangeError : num > 2");
	}
	else if (num < 0){
		CCLOG("SpriteChangeError : num < 0");
	}
	else{
		std::stringstream name;
		name << ch << num << ".png";
		std::string getname = name.str();
		sp->setTexture(Director::getInstance()->getTextureCache()->addImage(getname.c_str()));
	}
}
void returnfilename(){
	std::vector<std::string> fullfilename(1);
	fullfilename.at(0) = FileUtils::getInstance()->fullPathForFilename("image/BirdBody0.png");
	int slashstack = 0;
	while (slashstack != 5)
	{
		if (fullfilename.at(0).back() == '/'){
			slashstack++;
		}
		fullfilename.at(0).pop_back();
	}
	fullfilename.at(0) += "/Resources/res/";
	FileUtils::getInstance()->setSearchPaths(fullfilename);
}