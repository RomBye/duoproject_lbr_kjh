#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Option.h"
#include "HelloWorldScene.h"
#include "AppDelegate.h"

using namespace cocos2d;


Option::Option(Layer* layer){
	parentLayer = layer;
	makeBackGround(parentLayer);
	makeOptionWindow();
	makeSelectButtons();
	makeSoundBlock();
};

//BackGround를 그리는 함수 layer=현재 layer
void Option::makeBackGround(Layer*layer)
{
	backGround = DrawNode::create();
	backGround->setPosition(Vec2(0,0));
	backGround->drawSolidRect(Vec2(0, 0), Vec2(AppDelegate::resolution), Color4F(0.0f, 0.0f, 0.0f, 0.2f));
	layer->addChild(backGround);
	CCLOG("make background");
}


//Window를 그리는 함수
void Option::makeOptionWindow()
{
	Window = DrawNode::create();
	Window->setPosition(Vec2(0, 0));
	Window->drawSolidRect(Vec2((AppDelegate::resolution) * 0.2f), Vec2((AppDelegate::resolution) * 0.8f), Color4F(0.0f, 0.6f, 0.0f, 1.0f));
	this->backGround->addChild(Window);
	CCLOG("make window");
}


char*blockColor[4] = {
	"red",
	"green",
	"blue",
	"blink"
};

//SoundBlock을 그리는 함수
void Option::makeSoundBlock()
{
	
	for (int i = 0; i < 10; i++){
		std::stringstream ss;
		ss << "soundblock/" << "sound_" << blockColor[2] << "_" << i+1 << ".png";
		bgmSound[i] = Sprite::create(ss.str());
		bgmSound[i]->setScale(0.25f);
		bgmSound[i]->setPosition(Vec2(AppDelegate::resolution.width*0.43 + i * AppDelegate::resolution.width*0.03, AppDelegate::resolution.height*0.7));
		Window->addChild(bgmSound[i]);

		std::stringstream ss2;
		ss2 << "soundblock/" << "sound_" << blockColor[0] << "_" << i + 1 << ".png";
		effectSound[i] = Sprite::create(ss2.str());
		effectSound[i]->setScale(0.25f);
		effectSound[i]->setPosition(Vec2(AppDelegate::resolution.width*0.43 + i * AppDelegate::resolution.width*0.03, AppDelegate::resolution.height*0.6));
		Window->addChild(effectSound[i]);
	}

	member[0] = Label::createWithTTF("BGM", "04B20.ttf", 20.0f);
	member[0]->setPosition(bgmSound[0]->getPosition() - Vec2(AppDelegate::resolution.width*0.07, 0));
	Window->addChild(member[0]);
	member[1] = Label::createWithTTF("EFFECT", "04B20.ttf", 20.0f);
	member[1]->setPosition(effectSound[0]->getPosition() - Vec2(AppDelegate::resolution.width*0.1, 0));
	Window->addChild(member[1]);
}


char* selectButtonNames[3] = {
	"OK",
	"Apply",
	"CanCel"
};

//SelectButton을 그리는 함수
void Option::makeSelectButtons()
{
	for (int i = 0; i < 3; i++){
		selectChoiceButton[i] = ui::Button::create("image/btn_up.png");
		selectChoiceButton[i]->setTitleText(selectButtonNames[i]);
		selectChoiceButton[i]->setTitleColor(Color3B(255, 0, 0));
		selectChoiceButton[i]->setTitleFontSize(20.0f);
		selectChoiceButton[i]->setPosition(Vec2((AppDelegate::resolution.width)*(0.35 + i*0.15), AppDelegate::resolution.height*0.3));
		Window->addChild(selectChoiceButton[i]);
	}
	selectChoiceButton[2]->addClickEventListener(std::bind(&Option::optionClear, this, std::placeholders::_1));
}


//OK버튼 클릭시 변경사항을 적용시키고 창을 닫는 함수
void Option::optionOk()
{

}


//Apply버튼 클릭시 변경사항을 적용시키되 창을 닫지 않는 함수
void Option::optionApply()
{

}


//Cancle버튼 클릭시 변경사항을 저장하지 않고 창을 닫는 함수
void Option::optionClear(Ref*r)
{
	CCLOG("??");
//	Label* test = Label::createWithTTF("??", "04b20.ttf", 20.0f);
//	test->setPosition(Vec2(0,AppDelegate::resolution.height / 2));
//	Window->addChild(test);
	bgmSound[0]->removeFromParent();
}


//SoundBlock 클릭시 사운드를 조절하는 함수
void Option::volumeRegulate(int num, int size)
{

}

