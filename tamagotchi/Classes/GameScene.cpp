#include "cocos2d.h"
#include "ui\CocosGUI.h"

#include "GameScene.h"

using namespace cocos2d;

Scene* GameScene::createScene(bool StateRead){

	auto SceneGame = Scene::create();

	auto layer = GameScene::create();

	SceneGame->addChild(layer);
	layer->GameStartState = StateRead;

	return SceneGame;
}

bool GameScene::init(){
	if (!Layer::init()){
		return false;
	}
	return true;
}
void GameScene::onEnter(){
	Layer::onEnter();
	if (GameStartState)
	{
		Bird.LifeDataReceive();
		Bird.LifeSpriteBodyDefine(this);
		Bird.LifeButtonDefine(this);
		this->schedule(schedule_selector(GameScene::Change), 2.0f);
		this->schedule(schedule_selector(GameScene::Time), 2.0f);
		this->schedule(schedule_selector(GameScene::move), 0.03f);
	}
	else{
		Bird.LifeeggDefine(this);
		touchegg = EventListenerTouchOneByOne::create();
		touchegg->onTouchBegan = std::bind(&GameScene::eggTouchEvnet, this, std::placeholders::_1, std::placeholders::_2);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchegg, this);
	}
}
bool GameScene::eggTouchEvnet(Touch*, Event*){
	if (!Bird.SpriteClickegg()){
		Bird.LifeSpriteBodyDefine(this);
		Bird.LifeButtonDefine(this);
		this->schedule(schedule_selector(GameScene::Change), 2.0f);
		this->schedule(schedule_selector(GameScene::Time), 2.0f);
		this->schedule(schedule_selector(GameScene::move), 0.03f);
		touchegg->setEnabled(false);
	}
	CCLOG("end");
	return true;
}
void GameScene::Time(float dt){
	switch (Bird.getLifeFlesh()){
	case true:
		Bird.LifeTotalSchedule();
		break;
	default:
		Bird.LifeDiedScene(this);
		break;
	}
}

void GameScene::Change(float dt){
	switch (Bird.getLifeFlesh()){
	case true:
		Bird.LifeOverSchedule();
		break;
	default:
		Bird.LifeDiedScene(this);
		break;
	}
}
void GameScene::move(float){
	Bird.LifeEyerflicker();
}
