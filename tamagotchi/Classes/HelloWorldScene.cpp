

#include "HelloWorldScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "AppDelegate.h"
#include "GameScene.h"
#include "Option.h"



USING_NS_CC;

using namespace cocostudio::timeline;
Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

	// HelloWorld::onEnter()가 2회 호출되어 주석처리함 차후 문제생길시 1순위 체크바람
	//layer->onEnter();
	// return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    if ( !Layer::init() )
    {
        return false;
    }

	versionGrapher();
    return true;
}

//version의 데이터를 받아 화면에 출력해주는 함수
void HelloWorld::versionGrapher()
{
	std::stringstream versionString;
	versionString << "Ver " << "0" << "." << "0" << "." << AppDelegate::RunCount;
	version = Label::createWithTTF("", "04b20.ttf", 10.0f);
	version->setString(versionString.str());
	version->setAnchorPoint(Vec2(0.0f,1.3f));
	version->setPosition(Vec2(0, 768));
	addChild(version);
}

void HelloWorld::onEnter(){
	Layer::onEnter();
	createButtons();
}

//메인 버튼 이름
char* mainButtonNames[4] = {
	"START",
	"LOAD",
	"OPTION",
	"EXIT"
};

//옵션버튼 클릭시 호출되는 함수
void HelloWorld::optionButtonClick()
{
	for (int i = 0; i < 4; i++){
		mainButton[i]->setEnabled(false);
	}
	Option mainOption = Option(this);

}

//메인 레이어와 메인 버튼 생성함수
void HelloWorld::createButtons()
{
	mainLayout = ui::Layout::create();
	mainLayout->setPosition(Vec2(640, 384));
	mainLayout->setLayoutType(ui::Layout::Type::VERTICAL);
	mainLayout->setSize(Size(130, 240));
	mainLayout->setBackGroundColor(Color3B(100, 255, 100));
	mainLayout->setBackGroundColorOpacity(200);
	mainLayout->setBackGroundColorType(ui::Layout::BackGroundColorType::SOLID);
	mainLayout->setAnchorPoint(Vec2(0.5f, 0.5f));
	this->addChild(mainLayout);
	for (int i = 0; i < 4; i++){
		mainButton[i] = ui::Button::create("image/btn_up.png");
		mainButton[i]->setTitleText(mainButtonNames[i]);
		mainButton[i]->setTitleColor(Color3B(255, 0, 0));
		mainButton[i]->setTitleFontSize(20.0f);
		mainLayout->addChild(mainButton[i]);
	}

	mainButton[0]->addClickEventListener([](Ref* rf)->void{ Director::getInstance()->replaceScene(GameScene::createScene(false)); });
	mainButton[1]->addClickEventListener([](Ref* rf)->void{ Director::getInstance()->replaceScene(GameScene::createScene(true)); });
	mainButton[2]->addClickEventListener(std::bind(&HelloWorld::optionButtonClick,this));
	mainButton[3]->addClickEventListener([](Ref* r)->void{Director::getInstance()->end(); });
}


//옵션창을 닫을때 호출되는 함수
void HelloWorld::optionClear()
{ 

}