#ifndef __OPTION_H__
#define __OPTION_H__


#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "HelloWorldScene.h"

using namespace cocos2d;

class Option
{
public:
	Label*member[2];

	//확인 ,적용 ,취소
	ui::Button* selectChoiceButton[3];
	
	//resolution : 해상도
	//[0] : 현재 적용중인 해상도
	//[1] : 지원 해상도 1
	//[2] : 지원 해상도 2
	//[3] : 지원 해상도 3
	ui::Button* selectResolutionButton[4];
	
	//BGM 사운드크기 (버튼화)
	Sprite* bgmSound[10];

	//EFFECT 사운드크기 (버튼화)
	Sprite* effectSound[10];

	//게임플레이화면을 가려줄 반불투명한 사각형
	DrawNode* backGround;

	//옵션창 베이스
	DrawNode* Window;

	//옵션창이 켜진 현재 레이어
	Layer* parentLayer;

	//테마
//	Thema thema=RED;





	Option(Layer*);
	

	//BackGround를 그리는 함수
	void makeBackGround(Layer*);

	//Window를 그리는 함수
	void makeOptionWindow();

	//SoundBlock을 그리는 함수
	void makeSoundBlock();

	//SelectButton을 그리는 함수
	void makeSelectButtons();

	//OK버튼 클릭시 변경사항을 적용시키고 창을 닫는 함수
	void optionOk();

	//Apply버튼 클릭시 변경사항을 적용시키되 창을 닫지 않는 함수
	void optionApply();

	//Cancle버튼 클릭시 변경사항을 저장하지 않고 창을 닫는 함수
	void optionClear(Ref*r);

	//SoundBlock 클릭시 사운드를 조절하는 함수
	void volumeRegulate(int,int);


};









#endif