#include "AppDelegate.h"
#include "HelloWorldScene.h"
#include "cocos2d.h"

USING_NS_CC;
int AppDelegate::RunCount;
Size AppDelegate::resolution;
AppDelegate::AppDelegate() {
	RunCounting();
    resolution = Size(1280, 768);
}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::createWithRect("tamagotchi", Rect(0, 0, resolution.width, resolution.height));
        director->setOpenGLView(glview);
    }

    director->getOpenGLView()->setDesignResolutionSize(1280, 768, ResolutionPolicy::SHOW_ALL);

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    FileUtils::getInstance()->addSearchPath("res");

    // create a scene. it's an autorelease object
    auto scene = HelloWorld::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}


// runcount 읽기,쓰기,가산을 동시에 하는 함수
void AppDelegate::RunCounting(){
	auto fileutil = FileUtils::getInstance();

	// runCount 저장위치 = res/savePath
	std::string savePath = "runcount.txt";
	CCLOG("File Path : %s", savePath.c_str());


	CCLOG("file reed...");
	if (fileutil->isFileExist(savePath.c_str())){
		CCLOG("fileutil->isFileExist(savePath.c_str()): %d", fileutil->isFileExist(savePath.c_str()));
		FILE*fp = fopen(savePath.c_str(), "r");
		fread(&RunCount, sizeof(int), 1, fp);
		RunCount++;
		fclose(fp);
	}
	else{
		CCLOG("not find file , now create file...");
		RunCount = 1;
	}
	
	FILE*fp = fopen(savePath.c_str(), "w");
	CCLOG("file writing...");
	CCLOG("runcount: %d", RunCount);
	fwrite(&RunCount, sizeof(int), 1, fp);
	CCLOG("runcount save success");
	fclose(fp);

	CCLOG("now RunCount is %d...", RunCount);
	
}